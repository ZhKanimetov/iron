﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    public interface IItem
    {
        Dictionary<string, string> Description();
        int Price { get; set; }
    }
}
