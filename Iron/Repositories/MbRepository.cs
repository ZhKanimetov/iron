﻿using Iron.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Repositories
{
    public interface IMbRepository : IItemRepository<Mb>
    {
    }

    public class MbRepository : ItemRepository<Mb>, IMbRepository
    {
        public MbRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Mb;
        }
    }
}
