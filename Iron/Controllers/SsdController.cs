﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class SsdController : Controller, IEntityController
    {
        private readonly ISsdReposirory _ssdReposirory;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public SsdController(ISsdReposirory ssdReposirory, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _ssdReposirory = ssdReposirory;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(SsdViewModel model)
        {
            Ssd cooler = new Ssd()
            {
                FormFactor = model.FormFactor,
                Ifs = model.Ifs,
                ReadSpeed = model.ReadSpeed,
                Volume = model.Volume,
                WriteSpeed = model.WriteSpeed,
                Price = model.Price,
                Image = model.Image != null ? $"images/Ssd/{model.Image.FileName}" : $"images/Ssd/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Ssd").Id
            };
            _ssdReposirory.AddEntity(cooler);
            if (model.Image != null) _fileUploadService.Upload($"images\\Ssd", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _ssdReposirory.FirstOrDefault(x => x.Id == id);
            _ssdReposirory.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var ssd = _ssdReposirory.FirstOrDefault(x => x.Id == id);
            SsdViewModel model = new SsdViewModel()
            {
                Id = ssd.Id,
                FormFactor = ssd.FormFactor,
                Ifs = ssd.Ifs,
                ReadSpeed = ssd.ReadSpeed,
                Volume = ssd.Volume,
                WriteSpeed = ssd.WriteSpeed,
                Price = ssd.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(SsdViewModel model)
        {
            var entity = _ssdReposirory.FirstOrDefault(x => x.Id == model.Id);
            entity.FormFactor = model.FormFactor;
            entity.Ifs = model.Ifs;
            entity.ReadSpeed = model.ReadSpeed;
            entity.Volume = model.Volume;
            entity.WriteSpeed = model.WriteSpeed;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Ssd/{model.Image.FileName}" : entity.Image;
            _ssdReposirory.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Ssd", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_ssdReposirory.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_ssdReposirory.GetAllEntities().ToList());
        }
    }
}