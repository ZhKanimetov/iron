﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.ViewModels
{
    public class CoolerViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Размеры вентилятора")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Size { get; set; }
        [Display(Name = "Количество вентиляторов")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Amount { get; set; }
        [Display(Name = "Уровень шума")]
        [Required(ErrorMessage = "Заполните поле")]
        public int NoiseLevel { get; set; }
        [Display(Name = "Скорость вращения")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Speed { get; set; }
        [Display(Name = "Макс. тепловыделение процессора")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Heat { get; set; }
        [Display(Name = "Socket")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Socket { get; set; }
        [Display(Name = "Производитель")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Manufacturer { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
