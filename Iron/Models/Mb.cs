﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Mb : IItem
    {
        public int Id { get; set; }
        public string Socket { get; set; }
        public string Chipset { get; set; }
        public string FormFactor { get; set; }
        public string MemoryType { get; set; }
        public int Slots { get; set; }
        public int MaxMemory { get; set; }
        public string ExpansionSlots { get; set; }
        public string Sound { get; set; }
        public string Network { get; set; }
        public string RearConnectors { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Chipset} {FormFactor} {Socket}";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Socket", Socket);
            dic.Add("Чипсет", Chipset);
            dic.Add("Форм-фактор", FormFactor);
            dic.Add("Тип памяти", MemoryType);
            dic.Add("Количество слотов памяти", Slots.ToString());
            dic.Add("Максимальный объем памяти", MaxMemory.ToString());
            dic.Add("Слоты расширения", ExpansionSlots);
            dic.Add("Звук", Sound);
            dic.Add("Сетевой интерфейс", Network);
            dic.Add("Разъёмы на задней панели", RearConnectors);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
