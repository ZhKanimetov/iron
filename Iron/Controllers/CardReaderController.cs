﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class CardReaderController : Controller, IEntityController
    {
        private readonly ICardReaderRepository _cardReaderRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public CardReaderController(ICardReaderRepository cardReaderRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _cardReaderRepository = cardReaderRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CardReaderViewModel model)
        {
            CardReader cooler = new CardReader()
            {
                Name = model.Name,
                Slots = model.Slots,
                Price = model.Price,
                Image = model.Image != null ? $"images/CardReader/{model.Image.FileName}" : $"images/CardReader/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "CardReader").Id
            };
            _cardReaderRepository.AddEntity(cooler);
            if (model.Image != null) _fileUploadService.Upload($"images\\CardReader", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _cardReaderRepository.FirstOrDefault(x => x.Id == id);
            _cardReaderRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _cardReaderRepository.FirstOrDefault(x => x.Id == id);
            CardReaderViewModel model = new CardReaderViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Slots = entity.Slots,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(CardReaderViewModel model)
        {
            var entity = _cardReaderRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Name = model.Name;
            entity.Slots = model.Slots;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/CardReader/{model.Image.FileName}" : entity.Image;
            _cardReaderRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\CardReader", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_cardReaderRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_cardReaderRepository.GetAllEntities().ToList());
        }
    }
}