﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Ssd : IItem
    {
        public int Id { get; set; }
        public int ReadSpeed { get; set; }
        public int WriteSpeed { get; set; }
        public string Ifs { get; set; }
        public string FormFactor { get; set; }
        public int Volume { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Volume} SSD";
        }


        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Скорость чтения", ReadSpeed.ToString());
            dic.Add("Скорость записи", WriteSpeed.ToString());
            dic.Add("Интерфейс", Ifs);
            dic.Add("Форм-фактор SSD", FormFactor);
            dic.Add("Объем SSD накопителя", Volume.ToString());
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
