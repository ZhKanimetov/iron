﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Iron.ViewModels
{
    public class CardReaderViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Название")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Name { get; set; }
        [Display(Name = "Слоты для карт памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Slots { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
