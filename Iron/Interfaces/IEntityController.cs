﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Interfaces
{
    public interface IEntityController
    {
        IActionResult Index();
        IActionResult Delete(int id);
        IActionResult Edit(int id);
        IActionResult Create();
        IActionResult ToSelect();
    }
}
