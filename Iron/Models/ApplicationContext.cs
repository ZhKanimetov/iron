﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Iron.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Cooler> Cooler { get; set; }
        public DbSet<Cpu> Cpu { get; set; }
        public DbSet<Gpu> Gpu { get; set; }
        public DbSet<Hdd> Hdd { get; set; }
        public DbSet<Mb> Mb { get; set; }
        public DbSet<Mem> Mem { get; set; }
        public DbSet<Ssd> Ssd { get; set; }
        public DbSet<WiFi> WiFi { get; set; }
        public DbSet<Body> Body { get; set; }
        public DbSet<PowerSupply> PowerSupply { get; set; }
        public DbSet<Oc> Oc { get; set; }
        public DbSet<CardReader> CardReader { get; set; }
        public DbSet<Dvd> Dvd { get; set; }
        public DbSet<Choose> Choose { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(new Category
                {
                    Id=1,
                    Name ="Процессор",
                    Description = "Процессор (CPU) – сердце компьютера. Чем выше частота тем быстрее обрабатываются данные, а количество ядер позволяет распределить нагрузку и повысить быстродействие всей системы. Для разгона существуют модели с разблокированным множителем. Больший кеш улучшает работу в тяжелых режимах.",
                    IsImportant = true,
                    ImagePath = "images/categories/chip.png",
                    ControllerName = "Cpu"
                }, new Category
                {
                    Id=2,
                    Name ="Охлаждение",
                    Description = "Охлаждения (cooler) – радиатор с прикреплённым вентилятором предназначенный для охлаждения процессора. Показатель теплоотвода (TDP) кулера не должен быть меньше показателя тепловыделения (TDP) процессора. А если процессор выбираете для разгона, то теплоотвод системы охлаждения должен превышать показатель минимум в два раза.",
                    IsImportant = false,
                    ImagePath = "images/categories/cooler.png",
                    ControllerName = "Cooler"
                }, new Category
                {
                    Id=3,
                    Name ="Материнская плата",
                    Description = "Материнская плата (MB) – основа компьютера. На плату как конструктор собираются остальные комплектующие. Материнская плата не отвечает за быстродействие компьютера, но за функционал, от нее зависит количество нужных выходов и разъемов, поддержка режимов SLI, Cross-Fire, Raid, и конечно разводка системы питания.",
                    IsImportant = true,
                    ImagePath = "images/categories/motherboard.png",
                    ControllerName = "Mb"
                }, new Category
                {
                    Id=4,
                    Name ="Оперативная память",
                    Description = "Оперативная память (Mem) – отвечает за то, с каким объемом данных в данный момент времени может работать процессор. Чем ее больше, тем быстрее работает компьютер.",
                    IsImportant = true,
                    ImagePath = "images/categories/ram-memory.png",
                    ControllerName = "Mem"
                }, new Category
                {
                    Id=5,
                    Name ="Жёсткий диск",
                    Description = "Жесткий диск (HDD) – устройство для хранения данных, характеризуется объемом и скоростью (чтение/запись) чем больше номинальный объем тем больше данных поместится. Скорость записи и чтения влияет на копирование данных, запись и время отклика ОС.",
                    IsImportant = true,
                    ImagePath = "images/categories/hdd-storage.png",
                    ControllerName = "Hdd"
                }, new Category
                {
                    Id = 6,
                    Name = "Твердотельный накопитель",
                    Description = "Твердотельный накопитель (SSD) – это скоростное устройство для хранения данных. Его скорость работы в несколько раз быстрее обычного жесткого диска. Обычно SSD используют для установки на него Операционной системы и всех сопутствующих программ. Существенно ускоряет работу всего компьютера.",
                    IsImportant = false,
                    ImagePath = "images/categories/ssd-storage.png",
                    ControllerName = "Ssd"
                }, new Category
                {
                    Id = 7,
                    Name = "Видео-карта",
                    Description = "Видеокарта (GPU) – это устройство отвечающее за поддержку и быстродействие игрового процесса. Основой видеокарты есть графический чип, чем выше мощность тем лучше. Второстепенный показатель – это видеопамять, отвечающая за подгрузку текстур на дисплей.",
                    IsImportant = false,
                    ImagePath = "images/categories/gpu.png",
                    ControllerName = "Gpu"
                }, new Category
                {
                    Id = 8,
                    Name = "Оптический привод",
                    Description = "Оптический привод – устройство чтения и записи CD/DVD дисков. Привод Blu-Ray поддерживает чтение или чтение/запись Blu-Ray дисков, и чтение/запись любых CD/DVD дисков.",
                    IsImportant = false,
                    ImagePath = "images/categories/cd.png",
                    ControllerName = "Dvd"
                }, new Category
                {
                    Id = 9,
                    Name = "Кард-ридер",
                    Description = "Карт-ридер – это устройство считывания данных с карт памяти",
                    IsImportant = false,
                    ImagePath = "images/categories/card-reader.png",
                    ControllerName = "CardReader"
                }, new Category
                {
                    Id = 10,
                    Name = "Беспроводная связь",
                    Description = "Беспроводная связь – устройство подключения компьютера к сети интернет по беспроводному каналу связи Wi-Fi, через роутер или точку доступа.",
                    IsImportant = false,
                    ImagePath = "images/categories/wifi.png",
                    ControllerName = "WiFi"
                }, new Category
                {
                    Id = 11,
                    Name = "Корпус",
                    Description = "Корпус – не маловажная составляющая системного блока. Толщина стенок определяют прочность и шума-изоляцию. Размер влияет на охлаждение внутренних компонентов.",
                    IsImportant = false,
                    ImagePath = "images/categories/system-unit.png",
                    ControllerName = "Body"
                }, new Category
                {
                    Id = 12,
                    Name = "Блок питания",
                    Description = "Блок питания обеспечивает током все компоненты и противостоит всем перегрузкам и скачкам сети. Мощность блока питания выбирается всегда с запасом, так он дольше прослужит без пиковых нагрузок.",
                    IsImportant = true,
                    ImagePath = "images/categories/power.png",
                    ControllerName = "PowerSupply"
                }, new Category
                {
                    Id = 13,
                    Name = "Операционная система",
                    Description = "Операционная система (ОС) – это комплекс взаимосвязанных программ, предназначенных для управления ресурсами вычислительного устройства и организации взаимодействия с пользователем.",
                    IsImportant = false,
                    ImagePath = "images/categories/microsoft.png",
                    ControllerName = "Oc"
                });
            base.OnModelCreating(modelBuilder);
        }
    }
}
