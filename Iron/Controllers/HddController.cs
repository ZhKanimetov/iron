﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class HddController : Controller, IEntityController
    {
        private readonly IHddRepository _hddRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public HddController(IHddRepository hddRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _hddRepository = hddRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(HddViewModel model)
        {
            var entity = new Hdd()
            {
                FormFactor = model.FormFactor,
                Ifs = model.Ifs,
                Speed = model.Speed,
                Volume = model.Volume,
                Price = model.Price,
                Image = model.Image != null ? $"images/Hdd/{model.Image.FileName}" : $"images/Hdd/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Hdd").Id
            };
            _hddRepository.AddEntity(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Hdd", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _hddRepository.FirstOrDefault(x => x.Id == id);
            _hddRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var mb = _hddRepository.FirstOrDefault(x => x.Id == id);
            HddViewModel model = new HddViewModel()
            {
                Id = mb.Id,
                FormFactor = mb.FormFactor,
                Ifs = mb.Ifs,
                Speed = mb.Speed,
                Volume = mb.Volume,
                Price = mb.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(HddViewModel model)
        {
            var entity = _hddRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Volume = model.Volume;
            entity.Speed = model.Speed;
            entity.Ifs = model.Ifs;
            entity.FormFactor = model.FormFactor;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Hdd/{model.Image.FileName}" : entity.Image;
            _hddRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Hdd", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_hddRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_hddRepository.GetAllEntities().ToList());
        }
    }
}