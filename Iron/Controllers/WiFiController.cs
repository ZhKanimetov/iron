﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class WiFiController : Controller, IEntityController
    {
        private readonly IWiFiRepository _wiFiRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public WiFiController(IWiFiRepository wiFiRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _wiFiRepository = wiFiRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(WiFiVievModel model)
        {
            WiFi entity = new WiFi()
            {
                Antennas = model.Antennas,
                Ifs = model.Ifs,
                Standard = model.Standard,
                TypeAntennas = model.TypeAntennas,
                Speed = model.Speed,
                Price = model.Price,
                Image = model.Image != null ? $"images/WiFi/{model.Image.FileName}" : $"images/Cooler/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "WiFi").Id
            };
            _wiFiRepository.AddEntity(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\WiFi", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _wiFiRepository.FirstOrDefault(x => x.Id == id);
            _wiFiRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _wiFiRepository.FirstOrDefault(x => x.Id == id);
            WiFiVievModel model = new WiFiVievModel()
            {
                Id = entity.Id,
                Antennas = entity.Antennas,
                Ifs = entity.Ifs,
                Standard = entity.Standard,
                TypeAntennas = entity.TypeAntennas,
                Speed = entity.Speed,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(WiFiVievModel model)
        {
            var entity = _wiFiRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Antennas = model.Antennas;
            entity.Ifs = model.Ifs;
            entity.Standard = model.Standard;
            entity.TypeAntennas = model.TypeAntennas;
            entity.Speed = model.Speed;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/WiFi/{model.Image.FileName}" : entity.Image;
            _wiFiRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\WiFi", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_wiFiRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_wiFiRepository.GetAllEntities().ToList());
        }
    }
}