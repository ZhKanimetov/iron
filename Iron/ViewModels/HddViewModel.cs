﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.ViewModels
{
    public class HddViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Скорость вращения жесткого диска")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Speed { get; set; }
        [Display(Name = "Интерфейс")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Ifs { get; set; }
        [Display(Name = "Форм-фактор HDD")]
        [Required(ErrorMessage = "Заполните поле")]
        public string FormFactor { get; set; }
        [Display(Name = "Объем жесткого диска")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Volume { get; set; }
        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
