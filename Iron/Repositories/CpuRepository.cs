﻿using Iron.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Repositories
{
    public interface ICpuRepository : IItemRepository<Cpu>
    {
    }

    public class CpuRepository : ItemRepository<Cpu>, ICpuRepository
    {
        public CpuRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Cpu;
        }
    }
}
