﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Iron.ViewModels
{
    public class BodyViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Типоразмер")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Type { get; set; }
        [Display(Name = "Форм-фактор")]
        [Required(ErrorMessage = "Заполните поле")]
        public string FormFactor { get; set; }
        [Display(Name = "Интерфейсы на лицевой панели")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Ifs { get; set; }
        [Display(Name = "Цвет")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Color { get; set; }
        [Display(Name = "Габариты")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Dimensions { get; set; }
        [Display(Name = "Материал корпуса")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Material { get; set; }
        [Display(Name = "Производитель")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Manufacturer { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
