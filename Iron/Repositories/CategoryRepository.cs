﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;
using Iron.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Iron.Repositories
{
    public interface ICategoryRepository : IItemRepository<Category>
    {
        ChooseViewModel GetSelected(List<string> list);
    }

    public class CategoryRepository : ItemRepository<Category>, ICategoryRepository
    {
        private readonly ApplicationContext _context;

        public CategoryRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Categories;
            _context = context;
        }

        public ChooseViewModel GetSelected(List<string> list)
        {
            var dic = list.ToDictionary(x => x.Split(":").First(), x=> x.Split(":").Last());

            ChooseViewModel model = new ChooseViewModel
            {
                Body = _context.Body.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Body").Id.ToString()).Value),
                CardReader = _context.CardReader.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "CardReader").Id.ToString()).Value),
                Cooler = _context.Cooler.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Cooler").Id.ToString()).Value),
                Cpu = _context.Cpu.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Cpu").Id.ToString()).Value),
                Dvd = _context.Dvd.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Dvd").Id.ToString()).Value),
                Gpu = _context.Gpu.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Gpu").Id.ToString()).Value),
                Hdd = _context.Hdd.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Hdd").Id.ToString()).Value),
                Mb = _context.Mb.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Mb").Id.ToString()).Value),
                Mem = _context.Mem.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Mem").Id.ToString()).Value),
                Oc = _context.Oc.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Oc").Id.ToString()).Value),
                Ssd = _context.Ssd.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "Ssd").Id.ToString()).Value),
                PowerSupply = _context.PowerSupply.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "PowerSupply").Id.ToString()).Value),
                Wifi = _context.WiFi.Include(x => x.Category).FirstOrDefault(x =>
                    x.Id.ToString() == dic.FirstOrDefault(d =>
                        d.Key == AllEntities.First(c => c.ControllerName == "WiFi").Id.ToString()).Value)
            };



            return model;
        }
    }
}
