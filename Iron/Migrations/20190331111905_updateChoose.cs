﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iron.Migrations
{
    public partial class updateChoose : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Choose",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "Content",
                table: "Choose",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
