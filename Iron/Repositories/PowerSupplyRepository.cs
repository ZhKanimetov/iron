﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IPowerSupplyRepository : IItemRepository<PowerSupply>
    {
    }

    public class PowerSupplyRepository : ItemRepository<PowerSupply>, IPowerSupplyRepository
    {
        public PowerSupplyRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.PowerSupply;
        }
    }
}
