﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.ViewModels
{
    public class MbViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Socket")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Socket { get; set; }

        [Display(Name = "Чипсет")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Chipset { get; set; }

        [Display(Name = "Форм-фактор")]
        [Required(ErrorMessage = "Заполните поле")]
        public string FormFactor { get; set; }

        [Display(Name = "Тип памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public string MemoryType { get; set; }

        [Display(Name = "Количество слотов памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Slots { get; set; }

        [Display(Name = "Максимальный объем памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public int MaxMemory { get; set; }

        [Display(Name = "Слоты расширения")]
        [Required(ErrorMessage = "Заполните поле")]
        public string ExpansionSlots { get; set; }

        [Display(Name = "Звук")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Sound { get; set; }

        [Display(Name = "Сетевой интерфейс")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Network { get; set; }
        [Display(Name = "Разъёмы на задней панели")]
        [Required(ErrorMessage = "Заполните поле")]
        public string RearConnectors { get; set; }


        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
