﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IWiFiRepository : IItemRepository<WiFi>
    {
    }

    public class WiFiRepository : ItemRepository<WiFi>, IWiFiRepository
    {
        public WiFiRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.WiFi;
        }
    }

}
