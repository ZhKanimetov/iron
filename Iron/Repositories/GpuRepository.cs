﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IGpuRepository : IItemRepository<Gpu>
    {
    }

    public class GpuRepository : ItemRepository<Gpu>, IGpuRepository
    {
        public GpuRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Gpu;
        }
    }
}
