﻿using Iron.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Iron.Repositories
{
    public interface IItemRepository<T> where T : class
    {
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAllEntities();
        T AddEntity(T entity);
        void RemoveEntity(T entity);
        T GetFirst();
        void Update(T entity);
    }

    public class ItemRepository<T> : IItemRepository<T> where T : class
    {
        private readonly ApplicationContext context;

        protected DbSet<T> AllEntities { get; set; }

        public ItemRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public virtual T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {

            return AllEntities.FirstOrDefault(predicate);
        }

        public virtual IEnumerable<T> GetAllEntities()
        {
            return AllEntities.ToList();
        }

        public T AddEntity(T entity)
        {
            var model = AllEntities.Add(entity).Entity;
            context.SaveChanges();
            return model;
        }

        public void RemoveEntity(T entity)
        {
            AllEntities.Remove(entity);
            context.SaveChanges();
        }

        public T GetFirst()
        {
            return AllEntities.FirstOrDefault();
        }

        public void Update(T entity)
        {
            AllEntities.Update(entity);
            context.SaveChanges();
        }
    }
}
