﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class DvdController : Controller, IEntityController
    {
        private readonly IDvdRepository _dvdRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public DvdController(IDvdRepository dvdRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _dvdRepository = dvdRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(DvdViewModel model)
        {
            Dvd cooler = new Dvd()
            {
                Name = model.Name,
                Read = model.Read,
                Write = model.Write,
                Price = model.Price,
                Image = model.Image != null ? $"images/Dvd/{model.Image.FileName}" : $"images/Dvd/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Dvd").Id
            };
            _dvdRepository.AddEntity(cooler);
            if (model.Image != null) _fileUploadService.Upload($"images\\Dvd", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _dvdRepository.FirstOrDefault(x => x.Id == id);
            _dvdRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _dvdRepository.FirstOrDefault(x => x.Id == id);
            DvdViewModel model = new DvdViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Read = entity.Read,
                Write = entity.Write,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(DvdViewModel model)
        {
            var entity = _dvdRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Name = model.Name;
            entity.Read = model.Read;
            entity.Write = model.Write;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Dvd/{model.Image.FileName}" : entity.Image;
            _dvdRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Dvd", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_dvdRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_dvdRepository.GetAllEntities().ToList());
        }
    }
}