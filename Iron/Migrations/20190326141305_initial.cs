﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iron.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    IsImportant = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    ControllerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Choose",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Content = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Choose", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Body",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(nullable: true),
                    FormFactor = table.Column<string>(nullable: true),
                    Ifs = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true),
                    Dimensions = table.Column<string>(nullable: true),
                    Material = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Body", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Body_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CardReader",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Slots = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardReader", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardReader_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cooler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Size = table.Column<string>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    NoiseLevel = table.Column<int>(nullable: false),
                    Speed = table.Column<int>(nullable: false),
                    Heat = table.Column<int>(nullable: false),
                    Socket = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cooler", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cooler_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cpu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProcessorLine = table.Column<string>(nullable: true),
                    Model = table.Column<string>(nullable: true),
                    Socket = table.Column<string>(nullable: true),
                    Core = table.Column<string>(nullable: true),
                    NumberOfCores = table.Column<int>(nullable: false),
                    Speed = table.Column<int>(nullable: false),
                    CacheSize = table.Column<int>(nullable: false),
                    GraphicsCore = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cpu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cpu_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Dvd",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Read = table.Column<string>(nullable: true),
                    Write = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dvd", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dvd_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Gpu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Chipset = table.Column<string>(nullable: true),
                    Capacity = table.Column<int>(nullable: false),
                    MemoryFrequency = table.Column<int>(nullable: false),
                    ProcessorFrequency = table.Column<int>(nullable: false),
                    MemoryType = table.Column<string>(nullable: true),
                    Speed = table.Column<int>(nullable: false),
                    Ifs = table.Column<string>(nullable: true),
                    MinimumPower = table.Column<int>(nullable: false),
                    VideoCardType = table.Column<string>(nullable: true),
                    Cooler = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gpu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gpu_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Hdd",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Speed = table.Column<int>(nullable: false),
                    Ifs = table.Column<string>(nullable: true),
                    FormFactor = table.Column<string>(nullable: true),
                    Volume = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hdd", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hdd_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Mb",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Socket = table.Column<string>(nullable: true),
                    Chipset = table.Column<string>(nullable: true),
                    FormFactor = table.Column<string>(nullable: true),
                    MemoryType = table.Column<string>(nullable: true),
                    Slots = table.Column<int>(nullable: false),
                    MaxMemory = table.Column<int>(nullable: false),
                    ExpansionSlots = table.Column<string>(nullable: true),
                    Sound = table.Column<string>(nullable: true),
                    Network = table.Column<string>(nullable: true),
                    RearConnectors = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mb", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mb_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Mem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemoryType = table.Column<string>(nullable: true),
                    Speed = table.Column<int>(nullable: false),
                    Memory = table.Column<int>(nullable: false),
                    Socket = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mem_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Oc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Oc", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Oc_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PowerSupply",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FormFactor = table.Column<string>(nullable: true),
                    Power = table.Column<int>(nullable: false),
                    Cooler = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PowerSupply", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PowerSupply_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ssd",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReadSpeed = table.Column<int>(nullable: false),
                    WriteSpeed = table.Column<int>(nullable: false),
                    Ifs = table.Column<string>(nullable: true),
                    FormFactor = table.Column<string>(nullable: true),
                    Volume = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ssd", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ssd_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WiFi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Standard = table.Column<string>(nullable: true),
                    Speed = table.Column<int>(nullable: false),
                    Antennas = table.Column<string>(nullable: true),
                    TypeAntennas = table.Column<string>(nullable: true),
                    Ifs = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WiFi", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WiFi_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "ControllerName", "Description", "ImagePath", "IsImportant", "Name" },
                values: new object[,]
                {
                    { 1, "Cpu", "Процессор (CPU) – сердце компьютера. Чем выше частота тем быстрее обрабатываются данные, а количество ядер позволяет распределить нагрузку и повысить быстродействие всей системы. Для разгона существуют модели с разблокированным множителем. Больший кеш улучшает работу в тяжелых режимах.", "images/categories/chip.png", true, "Процессор" },
                    { 2, "Cooler", "Охлаждения (cooler) – радиатор с прикреплённым вентилятором предназначенный для охлаждения процессора. Показатель теплоотвода (TDP) кулера не должен быть меньше показателя тепловыделения (TDP) процессора. А если процессор выбираете для разгона, то теплоотвод системы охлаждения должен превышать показатель минимум в два раза.", "images/categories/cooler.png", false, "Охлаждение" },
                    { 3, "Mb", "Материнская плата (MB) – основа компьютера. На плату как конструктор собираются остальные комплектующие. Материнская плата не отвечает за быстродействие компьютера, но за функционал, от нее зависит количество нужных выходов и разъемов, поддержка режимов SLI, Cross-Fire, Raid, и конечно разводка системы питания.", "images/categories/motherboard.png", true, "Материнская плата" },
                    { 4, "Mem", "Оперативная память (Mem) – отвечает за то, с каким объемом данных в данный момент времени может работать процессор. Чем ее больше, тем быстрее работает компьютер.", "images/categories/ram-memory.png", true, "Оперативная память" },
                    { 5, "Hdd", "Жесткий диск (HDD) – устройство для хранения данных, характеризуется объемом и скоростью (чтение/запись) чем больше номинальный объем тем больше данных поместится. Скорость записи и чтения влияет на копирование данных, запись и время отклика ОС.", "images/categories/hdd-storage.png", true, "Жёсткий диск" },
                    { 6, "Ssd", "Твердотельный накопитель (SSD) – это скоростное устройство для хранения данных. Его скорость работы в несколько раз быстрее обычного жесткого диска. Обычно SSD используют для установки на него Операционной системы и всех сопутствующих программ. Существенно ускоряет работу всего компьютера.", "images/categories/ssd-storage.png", false, "Твердотельный накопитель" },
                    { 7, "Gpu", "Видеокарта (GPU) – это устройство отвечающее за поддержку и быстродействие игрового процесса. Основой видеокарты есть графический чип, чем выше мощность тем лучше. Второстепенный показатель – это видеопамять, отвечающая за подгрузку текстур на дисплей.", "images/categories/gpu.png", false, "Видео-карта" },
                    { 8, "Dvd", "Оптический привод – устройство чтения и записи CD/DVD дисков. Привод Blu-Ray поддерживает чтение или чтение/запись Blu-Ray дисков, и чтение/запись любых CD/DVD дисков.", "images/categories/cd.png", false, "Оптический привод" },
                    { 9, "CardReader", "Карт-ридер – это устройство считывания данных с карт памяти", "images/categories/card-reader.png", false, "Кард-ридер" },
                    { 10, "WiFi", "Беспроводная связь – устройство подключения компьютера к сети интернет по беспроводному каналу связи Wi-Fi, через роутер или точку доступа.", "images/categories/wifi.png", false, "Беспроводная связь" },
                    { 11, "Body", "Корпус – не маловажная составляющая системного блока. Толщина стенок определяют прочность и шума-изоляцию. Размер влияет на охлаждение внутренних компонентов.", "images/categories/system-unit.png", false, "Корпус" },
                    { 12, "PowerSupply", "Блок питания обеспечивает током все компоненты и противостоит всем перегрузкам и скачкам сети. Мощность блока питания выбирается всегда с запасом, так он дольше прослужит без пиковых нагрузок.", "images/categories/power.png", true, "Блок питания" },
                    { 13, "Oc", "Операционная система (ОС) – это комплекс взаимосвязанных программ, предназначенных для управления ресурсами вычислительного устройства и организации взаимодействия с пользователем.", "images/categories/microsoft.png", false, "Операционная система" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Body_CategoryId",
                table: "Body",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CardReader_CategoryId",
                table: "CardReader",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cooler_CategoryId",
                table: "Cooler",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cpu_CategoryId",
                table: "Cpu",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Dvd_CategoryId",
                table: "Dvd",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Gpu_CategoryId",
                table: "Gpu",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Hdd_CategoryId",
                table: "Hdd",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Mb_CategoryId",
                table: "Mb",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Mem_CategoryId",
                table: "Mem",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Oc_CategoryId",
                table: "Oc",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PowerSupply_CategoryId",
                table: "PowerSupply",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Ssd_CategoryId",
                table: "Ssd",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_WiFi_CategoryId",
                table: "WiFi",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Body");

            migrationBuilder.DropTable(
                name: "CardReader");

            migrationBuilder.DropTable(
                name: "Choose");

            migrationBuilder.DropTable(
                name: "Cooler");

            migrationBuilder.DropTable(
                name: "Cpu");

            migrationBuilder.DropTable(
                name: "Dvd");

            migrationBuilder.DropTable(
                name: "Gpu");

            migrationBuilder.DropTable(
                name: "Hdd");

            migrationBuilder.DropTable(
                name: "Mb");

            migrationBuilder.DropTable(
                name: "Mem");

            migrationBuilder.DropTable(
                name: "Oc");

            migrationBuilder.DropTable(
                name: "PowerSupply");

            migrationBuilder.DropTable(
                name: "Ssd");

            migrationBuilder.DropTable(
                name: "WiFi");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
