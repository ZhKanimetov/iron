﻿
using System;
using System.Collections.Generic;


namespace Iron.Models
{
    [Serializable]
    public class Gpu : IItem
    {
        public int Id { get; set; }
        public string Chipset { get; set; }
        public int Capacity { get; set; }
        public int MemoryFrequency { get; set; }
        public int ProcessorFrequency { get; set; }
        public string MemoryType { get; set; }
        public int Speed { get; set; }
        public string Ifs { get; set; }
        public int MinimumPower { get; set; }
        public string VideoCardType { get; set; }
        public string Cooler { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }


        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Chipset} {Capacity} Гб";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Чипсет", Chipset);
            dic.Add("Объем видеопамяти", Capacity.ToString());
            dic.Add("Частота памяти видеокарты", MemoryFrequency.ToString());
            dic.Add("Частота видеопроцессора", ProcessorFrequency.ToString());
            dic.Add("Тип видеопамяти", MemoryType);
            dic.Add("Шина / пропускная способность", Speed.ToString());
            dic.Add("Минимальная мощность БП", MinimumPower.ToString());
            dic.Add("Тип видеокарты", VideoCardType);
            dic.Add("Система охлаждения", Cooler);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
