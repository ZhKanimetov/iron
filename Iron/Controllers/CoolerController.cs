﻿using System.Linq;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class CoolerController : Controller, IEntityController
    {
        private readonly ICoolerRepository _coolerRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public CoolerController(ICoolerRepository coolerRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _coolerRepository = coolerRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CoolerViewModel model)
        {
            Cooler cooler = new Cooler()
            {
                Amount = model.Amount,
                Heat = model.Heat,
                Manufacturer = model.Manufacturer,
                NoiseLevel = model.NoiseLevel,
                Size = model.Size,
                Socket = model.Socket,
                Speed = model.Speed,
                Price = model.Price,
                Image = model.Image != null ? $"images/Cooler/{model.Image.FileName}" : $"images/Cooler/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Cooler").Id
            };
            _coolerRepository.AddEntity(cooler);
            if(model.Image != null) _fileUploadService.Upload($"images\\Cooler", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _coolerRepository.FirstOrDefault(x => x.Id == id);
            _coolerRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _coolerRepository.FirstOrDefault(x => x.Id == id);
            CoolerViewModel model = new CoolerViewModel()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                Heat = entity.Heat,
                Manufacturer = entity.Manufacturer,
                NoiseLevel = entity.NoiseLevel,
                Size = entity.Size,
                Socket = entity.Socket,
                Speed = entity.Speed,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(CoolerViewModel model)
        {
            var entity = _coolerRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Amount = model.Amount;
            entity.Heat = model.Heat;
            entity.Manufacturer = model.Manufacturer;
            entity.NoiseLevel = model.NoiseLevel;
            entity.Size = model.Size;
            entity.Socket = model.Socket;
            entity.Speed = model.Speed;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Cooler/{model.Image.FileName}" : entity.Image;
            _coolerRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Cooler", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_coolerRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_coolerRepository.GetAllEntities().ToList());
        }
    }
}