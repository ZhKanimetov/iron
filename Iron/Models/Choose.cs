﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    public class Choose
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Content { get; set; }
        public bool IsPublic { get; set; }
    }
}
