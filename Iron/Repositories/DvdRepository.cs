﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IDvdRepository : IItemRepository<Dvd>
    {
    }

    public class DvdRepository : ItemRepository<Dvd>, IDvdRepository
    {
        public DvdRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Dvd;
        }
    }
}
