﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Iron.Repositories;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class ManageController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IChooseRepository _chooseRepository;

        public ManageController(ICategoryRepository categoryRepository, IChooseRepository chooseRepository)
        {
            _categoryRepository = categoryRepository;
            _chooseRepository = chooseRepository;
        }

        public IActionResult Index()
        {
            return View(_categoryRepository.GetAllEntities());
        }

        public IActionResult Items(int categoryId)
        {
            var category = _categoryRepository.FirstOrDefault(x => x.Id == categoryId);

            if (category != null) return RedirectToAction("Index", category.ControllerName);

            return RedirectToAction("Index");
        }

        public IActionResult Orders()
        {
            List<ChooseViewModel> result = new List<ChooseViewModel>();
            var model = _chooseRepository.GetAllEntities().Where(x => x.IsPublic).ToList();
            for (int i = 0; i < model.Count(); i++)
            {
                var choose = _categoryRepository.GetSelected(model[i].Content.Split(",").ToList());
                choose.Name = model[i].Name;
                choose.Id = model[i].Id;
                result.Add(choose);
            }
            return View(result);
        }

        public IActionResult Delete(int id)
        {
            var entity = _chooseRepository.FirstOrDefault(x => x.Id == id);
            _chooseRepository.RemoveEntity(entity);
            return RedirectToAction("Orders");
        }
    }
}