﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface ICardReaderRepository : IItemRepository<CardReader>
    {
    }

    public class CardReaderRepository : ItemRepository<CardReader>, ICardReaderRepository
    {
        public CardReaderRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.CardReader;
        }
    }
}
