﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Iron.Models;
using Iron.Repositories;
using Iron.ViewModels;
using Microsoft.AspNetCore.Hosting;

namespace Iron.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IChooseRepository _chooseRepository;
        private readonly IHostingEnvironment _environment;


        public HomeController(ICategoryRepository categoryRepository, IChooseRepository chooseRepository, IHostingEnvironment environment)
        {
            _categoryRepository = categoryRepository;
            _chooseRepository = chooseRepository;
            _environment = environment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Collect()
        {
            var categories = _categoryRepository.GetAllEntities();
            ViewBag.Choose = _chooseRepository.GetAllEntities().Where(x => !x.IsPublic);
            return View(categories);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Choose(string list)
        {
            ChooseViewModel model = _categoryRepository.GetSelected(!string.IsNullOrEmpty(list) ? list.Split(",").ToList() : new List<string>());
            return PartialView("Choose", model);
        }

        [HttpPost]
        public void Order(List<string> list, string userName)
        {
            _chooseRepository.AddEntity(new Choose()
            {
                Name = userName,
                Content = string.Join(",", list),
                IsPublic = !User.Identity.IsAuthenticated
            });
        }

        [HttpPost]
        public void Print(List<string> list)
        {
            string fullPath = Path.Combine(_environment.WebRootPath, "Files.txt");
            ChooseViewModel model = _categoryRepository.GetSelected(list.First().Split(",").ToList());
            Type tModelType = model.GetType();
            PropertyInfo[] arrayPropertyInfos = tModelType.GetProperties();
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(fullPath))
            {
                int index = 1; 
                foreach (PropertyInfo property in arrayPropertyInfos)
                {
                    
                    IItem obj = null;
                    if (property.GetValue(model) is IItem)
                    {
                        obj = (IItem)property.GetValue(model);
                    }

                    if (obj != null)
                    {
                        file.WriteLine($"{index}) {obj.GetType().Name} - {obj.ToString()}  цена {obj.Price} сом");
                        foreach (var dic in obj.Description())
                        {
                            file.WriteLine($"\t{dic.Key} - {dic.Value}");
                        }
                        index++;
                    }

                    
                }
            }
        }

        public IActionResult Download()
        {
            string fullPath = Path.Combine(_environment.WebRootPath, "Files.txt");
            return PhysicalFile(fullPath, "application/txt", "Ващ выбор.txt");
        }
    }
}
