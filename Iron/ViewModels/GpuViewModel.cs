﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Iron.ViewModels
{
    public class GpuViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Чипсет")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Chipset { get; set; }
        [Display(Name = "Объем видеопамяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Capacity { get; set; }
        [Display(Name = "Частота памяти видеокарты")]
        [Required(ErrorMessage = "Заполните поле")]
        public int MemoryFrequency { get; set; }
        [Display(Name = "Частота видеопроцессора")]
        [Required(ErrorMessage = "Заполните поле")]
        public int ProcessorFrequency { get; set; }
        [Display(Name = "Тип видеопамяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public string MemoryType { get; set; }
        [Display(Name = "Шина / пропускная способность")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Speed { get; set; }
        [Display(Name = "Интерфейсы")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Ifs { get; set; }
        [Display(Name = "Минимальная мощность БП")]
        [Required(ErrorMessage = "Заполните поле")]
        public int MinimumPower { get; set; }
        [Display(Name = "Тип видеокарты")]
        [Required(ErrorMessage = "Заполните поле")]
        public string VideoCardType { get; set; }
        [Display(Name = "Система охлаждения")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Cooler { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
