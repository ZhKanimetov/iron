﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IBodyRepository : IItemRepository<Body>
    {
    }

    public class BodyRepository : ItemRepository<Body>, IBodyRepository
    {
        public BodyRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Body;
        }
    }
}
