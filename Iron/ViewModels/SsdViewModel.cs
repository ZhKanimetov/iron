﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Iron.ViewModels
{
    public class SsdViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Скорость чтения")]
        [Required(ErrorMessage = "Заполните поле")]
        public int ReadSpeed { get; set; }
        [Display(Name = "Скорость записи")]
        [Required(ErrorMessage = "Заполните поле")]
        public int WriteSpeed { get; set; }
        [Display(Name = "Интерфейс")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Ifs { get; set; }
        [Display(Name = "Форм-фактор SSD")]
        [Required(ErrorMessage = "Заполните поле")]
        public string FormFactor { get; set; }
        [Display(Name = "Объем SSD накопителя")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Volume { get; set; }
        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
