﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Iron.ViewModels
{
    public class PowerSupplyViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Форм-фактор")]
        [Required(ErrorMessage = "Заполните поле")]
        public string FormFactor { get; set; }
        [Display(Name = "Мощность")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Power { get; set; }
        [Display(Name = "Система охлаждения")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Cooler { get; set; }
        [Display(Name = "Тип разъема для материнской платы")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Type { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
    }
}
