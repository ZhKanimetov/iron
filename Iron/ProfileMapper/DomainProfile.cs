﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Iron.Models;
using Iron.ViewModels;

namespace Iron.ProfileMapper
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<CoolerViewModel, Cooler>();
        }
    }
}
