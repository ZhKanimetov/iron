﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class GpuController : Controller, IEntityController
    {
        private readonly IGpuRepository _gpuRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public GpuController(IGpuRepository gpuRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _gpuRepository = gpuRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(GpuViewModel model)
        {
            var entity = new Gpu()
            {
                Ifs = model.Ifs,
                Capacity = model.Capacity,
                Chipset = model.Chipset,
                Cooler = model.Cooler,
                MinimumPower = model.MinimumPower,
                MemoryType = model.MemoryType,
                VideoCardType = model.VideoCardType,
                MemoryFrequency = model.MemoryFrequency,
                ProcessorFrequency = model.ProcessorFrequency,
                Speed = model.Speed,
                Price = model.Price,
                Image = model.Image != null ? $"images/Gpu/{model.Image.FileName}" : $"images/Gpu/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Gpu").Id
            };
            _gpuRepository.AddEntity(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Gpu", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _gpuRepository.FirstOrDefault(x => x.Id == id);
            _gpuRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _gpuRepository.FirstOrDefault(x => x.Id == id);
            GpuViewModel model = new GpuViewModel()
            {
                Ifs = entity.Ifs,
                Capacity = entity.Capacity,
                Chipset = entity.Chipset,
                Cooler = entity.Cooler,
                MinimumPower = entity.MinimumPower,
                MemoryType = entity.MemoryType,
                VideoCardType = entity.VideoCardType,
                MemoryFrequency = entity.MemoryFrequency,
                ProcessorFrequency = entity.ProcessorFrequency,
                Speed = entity.Speed,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(GpuViewModel model)
        {
            var entity = _gpuRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Ifs = model.Ifs;
            entity.Capacity = model.Capacity;
            entity.Chipset = model.Chipset;
            entity.Cooler = model.Cooler;
            entity.MinimumPower = model.MinimumPower;
            entity.MemoryType = model.MemoryType;
            entity.VideoCardType = model.VideoCardType;
            entity.MemoryFrequency = model.MemoryFrequency;
            entity.ProcessorFrequency = model.ProcessorFrequency;
            entity.Speed = model.Speed;
            entity.Price = model.Price;

            entity.Image = model.Image != null ? $"images/Gpu/{model.Image.FileName}" : entity.Image;
            _gpuRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Gpu", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_gpuRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_gpuRepository.GetAllEntities().ToList());
        }
    }
}