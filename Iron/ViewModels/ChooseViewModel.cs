﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.ViewModels
{
    [Serializable]
    public class ChooseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Body Body { get; set; }
        public CardReader CardReader { get; set; }
        public Cooler Cooler { get; set; }
        public Cpu Cpu { get; set; }
        public Dvd Dvd { get; set; }
        public Gpu Gpu { get; set; }
        public Hdd Hdd { get; set; }
        public Mb Mb { get; set; }
        public Mem Mem { get; set; }
        public Oc Oc { get; set; }
        public PowerSupply PowerSupply { get; set; }
        public Ssd Ssd { get; set; }
        public WiFi Wifi { get; set; }
    }
}
