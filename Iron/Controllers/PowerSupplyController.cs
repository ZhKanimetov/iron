﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class PowerSupplyController : Controller, IEntityController
    {
        private readonly IPowerSupplyRepository _powerSupplyRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public PowerSupplyController(IPowerSupplyRepository powerSupplyRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _powerSupplyRepository = powerSupplyRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(PowerSupplyViewModel model)
        {
            PowerSupply cooler = new PowerSupply()
            {
                FormFactor = model.FormFactor,
                Cooler = model.Cooler,
                Power = model.Power,
                Type = model.Type,
                Price = model.Price,
                Image = model.Image != null ? $"images/PowerSupply/{model.Image.FileName}" : $"images/PowerSupply/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "PowerSupply").Id
            };
            _powerSupplyRepository.AddEntity(cooler);
            if (model.Image != null) _fileUploadService.Upload($"images\\PowerSupply", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _powerSupplyRepository.FirstOrDefault(x => x.Id == id);
            _powerSupplyRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _powerSupplyRepository.FirstOrDefault(x => x.Id == id);
            PowerSupplyViewModel model = new PowerSupplyViewModel()
            {
                Id = entity.Id,
                FormFactor = entity.FormFactor,
                Cooler = entity.Cooler,
                Power = entity.Power,
                Type = entity.Type,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(PowerSupplyViewModel model)
        {
            var entity = _powerSupplyRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.FormFactor = model.FormFactor;
            entity.Cooler = model.Cooler;
            entity.Power = model.Power;
            entity.Type = model.Type;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/PowerSupply/{model.Image.FileName}" : entity.Image;
            _powerSupplyRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\PowerSupply", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_powerSupplyRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_powerSupplyRepository.GetAllEntities().ToList());
        }
    }
}