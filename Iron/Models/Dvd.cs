﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Dvd
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Read { get; set; }
        public string Write { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Воспроизводит диски", Read);
            dic.Add("Записывает диски", Write);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
