﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IOcRepository : IItemRepository<Oc>
    {
    }

    public class OcRepository : ItemRepository<Oc>, IOcRepository
    {
        public OcRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Oc;
        }
    }
}
