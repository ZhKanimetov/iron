﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class MemController : Controller, IEntityController
    {
        private readonly IMemRepository _memRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public MemController(IMemRepository memRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _memRepository = memRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(MemViewModel model)
        {
            var entity = new Mem()
            {
                Memory = model.Memory,
                MemoryType = model.MemoryType,
                Speed = model.Speed,
                Socket = model.Socket,
                Price = model.Price,
                Image = model.Image != null ? $"images/Mem/{model.Image.FileName}" : $"images/Cooler/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Mem").Id
            };
            _memRepository.AddEntity(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Mem", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _memRepository.FirstOrDefault(x => x.Id == id);
            _memRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var mb = _memRepository.FirstOrDefault(x => x.Id == id);
            MemViewModel model = new MemViewModel()
            {
                Id = mb.Id,
                Memory = mb.Memory,
                MemoryType = mb.MemoryType,
                Speed = mb.Speed,
                Socket = mb.Socket,
                Price = mb.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(MemViewModel model)
        {
            var entity = _memRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.MemoryType = model.MemoryType;
            entity.Socket = model.Socket;
            entity.Speed = model.Speed;
            entity.Memory = model.Memory;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Mem/{model.Image.FileName}" : entity.Image;
            _memRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Mem", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_memRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_memRepository.GetAllEntities().ToList());
        }
    }
}