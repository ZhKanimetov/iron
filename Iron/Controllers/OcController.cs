﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class OcController : Controller, IEntityController
    {
        private readonly IOcRepository _ocRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public OcController(IOcRepository ocRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _ocRepository = ocRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(OcViewModel model)
        {
            Oc cooler = new Oc()
            {
                Name = model.Name,
                Price = model.Price,
                Image = model.Image != null ? $"images/Oc/{model.Image.FileName}" : $"images/Oc/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Oc").Id
            };
            _ocRepository.AddEntity(cooler);
            if (model.Image != null) _fileUploadService.Upload($"images\\Oc", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _ocRepository.FirstOrDefault(x => x.Id == id);
            _ocRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _ocRepository.FirstOrDefault(x => x.Id == id);
            OcViewModel model = new OcViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(OcViewModel model)
        {
            var entity = _ocRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Name = model.Name;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Oc/{model.Image.FileName}" : entity.Image;
            _ocRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Oc", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_ocRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_ocRepository.GetAllEntities().ToList());
        }
    }
}