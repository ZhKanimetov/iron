﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class PowerSupply : IItem
    {
        public int Id { get; set; }
        public string FormFactor { get; set; }
        public int Power { get; set; }
        public string Cooler { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"БП {Power} Вт";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Форм-фактор", FormFactor);
            dic.Add("Мощность", Power.ToString());
            dic.Add("Система охлаждения", Cooler);
            dic.Add("Тип разъема для материнской платы", Type);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
