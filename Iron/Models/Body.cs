﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Body : IItem
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string FormFactor { get; set; }
        public string Ifs { get; set; }
        public string Color { get; set; }
        public string Dimensions { get; set; }
        public string Material { get; set; }
        public string Manufacturer { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Manufacturer} {FormFactor}";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Типоразмер", Type);
            dic.Add("Форм-фактор", FormFactor);
            dic.Add("Интерфейсы на лицевой панели", Ifs);
            dic.Add("Цвет", Color);
            dic.Add("Габариты", Dimensions);
            dic.Add("Материал корпуса", Material);
            dic.Add("Производитель", Manufacturer);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
