﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface ISsdReposirory : IItemRepository<Ssd>
    {
    }

    public class SsdReposirory : ItemRepository<Ssd>, ISsdReposirory
    {
        public SsdReposirory(ApplicationContext context) : base(context)
        {
            AllEntities = context.Ssd;
        }
    }
}
