﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Mem : IItem
    {
        public int Id { get; set; }
        public string MemoryType { get; set; }
        public int Speed { get; set; }
        public int Memory { get; set; }
        public string Socket { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Memory} Гб {MemoryType} {Speed} МГц";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Тип памяти", MemoryType);
            dic.Add("Тактовая частота памяти", Speed.ToString());
            dic.Add("Объем памяти", Memory.ToString());
            dic.Add("Socket", Socket);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
