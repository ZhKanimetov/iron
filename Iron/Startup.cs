﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;

namespace Iron
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, IdentityRole>(
                    options =>
                    {
                        options.Password.RequireDigit = false;
                        options.Password.RequireLowercase = false;
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequiredLength = 3;
                    })
                .AddEntityFrameworkStores<ApplicationContext>();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<ICoolerRepository, CoolerRepository>();
            services.AddTransient<ICpuRepository, CpuRepository>();
            services.AddTransient<IMbRepository, MbRepository>();
            services.AddTransient<IMemRepository, MemRepository>();
            services.AddTransient<IHddRepository, HddRepository>();
            services.AddTransient<IGpuRepository, GpuRepository>();
            services.AddTransient<ISsdReposirory, SsdReposirory>();
            services.AddTransient<IBodyRepository, BodyRepository>();
            services.AddTransient<IOcRepository, OcRepository>();
            services.AddTransient<IDvdRepository, DvdRepository>();
            services.AddTransient<IPowerSupplyRepository, PowerSupplyRepository>();
            services.AddTransient<IWiFiRepository, WiFiRepository>();
            services.AddTransient<ICardReaderRepository, CardReaderRepository>();
            services.AddTransient<IChooseRepository, ChooseRepository>();
            services.AddTransient<FileUploadService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
