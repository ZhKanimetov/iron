﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Cpu : IItem
    {
        public int Id { get; set; }
        public string ProcessorLine { get; set; }
        public string Model { get; set; }
        public string Socket { get; set; }
        public string Core { get; set; }
        public int NumberOfCores { get; set; }
        public int Speed { get; set; }
        public int CacheSize { get; set; }
        public string GraphicsCore { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{ProcessorLine} {Model} {Speed} МГц";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Линейка процессора", ProcessorLine);
            dic.Add("Модель", Model);
            dic.Add("Socket", Socket);
            dic.Add("Ядро", Core);
            dic.Add("Количество ядер", NumberOfCores.ToString());
            dic.Add("Тактовая частота процессора", Speed.ToString());
            dic.Add("Объем кэша", CacheSize.ToString());
            dic.Add("Графическое ядро", GraphicsCore);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
