﻿using Iron.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Repositories
{
    public interface IHddRepository : IItemRepository<Hdd>
    {
    }

    public class HddRepository : ItemRepository<Hdd>, IHddRepository
    {
        public HddRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Hdd;
        }
    }
}
