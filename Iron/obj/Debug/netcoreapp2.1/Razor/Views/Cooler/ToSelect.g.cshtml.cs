#pragma checksum "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "84d4e1e8121ea549d31e98fb32fb886547140a36"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Cooler_ToSelect), @"mvc.1.0.view", @"/Views/Cooler/ToSelect.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Cooler/ToSelect.cshtml", typeof(AspNetCore.Views_Cooler_ToSelect))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "F:\Shamu\iron\Iron\Views\_ViewImports.cshtml"
using Iron;

#line default
#line hidden
#line 2 "F:\Shamu\iron\Iron\Views\_ViewImports.cshtml"
using Iron.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"84d4e1e8121ea549d31e98fb32fb886547140a36", @"/Views/Cooler/ToSelect.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3569a78c0bae86c10719f6e115a4c7fca8077e16", @"/Views/_ViewImports.cshtml")]
    public class Views_Cooler_ToSelect : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Iron.Models.Cooler>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("..."), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("width: 6em;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(40, 37, true);
            WriteLiteral("\r\n\r\n\r\n\r\n<ul class=\"media-list row\">\r\n");
            EndContext();
#line 7 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
     foreach (var part in Model)
    {

#line default
#line hidden
            BeginContext(118, 92, true);
            WriteLiteral("        <li class=\"media col-md-12\">\r\n            <div class=\"media-left\">\r\n                ");
            EndContext();
            BeginContext(210, 55, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "4e5de085c8714bd0b53c681589272c43", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            AddHtmlAttributeValue("", 220, "~/", 220, 2, true);
#line 11 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
AddHtmlAttributeValue("", 222, part.Image, 222, 11, false);

#line default
#line hidden
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(265, 102, true);
            WriteLiteral("\r\n            </div>\r\n            <div class=\"media-body\">\r\n                <h4 class=\"media-heading\">");
            EndContext();
            BeginContext(368, 15, false);
#line 14 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
                                     Write(part.ToString());

#line default
#line hidden
            EndContext();
            BeginContext(383, 28, true);
            WriteLiteral("</h4>\r\n                <p>\r\n");
            EndContext();
#line 16 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
                     foreach (var item in part.Description())
                    {

#line default
#line hidden
            BeginContext(497, 32, true);
            WriteLiteral("                        <strong>");
            EndContext();
            BeginContext(530, 8, false);
#line 18 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
                           Write(item.Key);

#line default
#line hidden
            EndContext();
            BeginContext(538, 12, true);
            WriteLiteral(":</strong>\r\n");
            EndContext();
            BeginContext(575, 10, false);
#line 19 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
                   Write(item.Value);

#line default
#line hidden
            EndContext();
#line 19 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
                                   

                    }

#line default
#line hidden
            BeginContext(612, 75, true);
            WriteLiteral("                </p>\r\n                <a class=\"btn btn-success pull-right\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 687, "\"", 781, 11);
            WriteAttributeValue("", 697, "select(\'", 697, 8, true);
#line 23 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
WriteAttributeValue("", 705, part.CategoryId, 705, 16, false);

#line default
#line hidden
            WriteAttributeValue("", 721, "\',\'", 721, 3, true);
#line 23 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
WriteAttributeValue("", 724, part.Id, 724, 8, false);

#line default
#line hidden
            WriteAttributeValue("", 732, "\',\'", 732, 3, true);
#line 23 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
WriteAttributeValue("", 735, part.Image, 735, 11, false);

#line default
#line hidden
            WriteAttributeValue("", 746, "\',\'", 746, 3, true);
#line 23 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
WriteAttributeValue("", 749, part.ToString(), 749, 16, false);

#line default
#line hidden
            WriteAttributeValue("", 765, "\',\'", 765, 3, true);
#line 23 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
WriteAttributeValue("", 768, part.Price, 768, 11, false);

#line default
#line hidden
            WriteAttributeValue("", 779, "\')", 779, 2, true);
            EndWriteAttribute();
            BeginContext(782, 49, true);
            WriteLiteral(">Выбрать</a>\r\n            </div>\r\n        </li>\r\n");
            EndContext();
#line 26 "F:\Shamu\iron\Iron\Views\Cooler\ToSelect.cshtml"
    }

#line default
#line hidden
            BeginContext(838, 13, true);
            WriteLiteral("    \r\n\r\n</ul>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Iron.Models.Cooler>> Html { get; private set; }
    }
}
#pragma warning restore 1591
