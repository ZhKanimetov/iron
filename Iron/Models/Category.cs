﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsImportant { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string ControllerName { get; set; }
    }
}
