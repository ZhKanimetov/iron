﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class CpuController : Controller, IEntityController
    {
        private readonly ICpuRepository _cpuRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public CpuController(ICpuRepository cpuRepository, ICategoryRepository categoryRepository, FileUploadService fileUploadService)
        {
            _cpuRepository = cpuRepository;
            _categoryRepository = categoryRepository;
            _fileUploadService = fileUploadService;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CpuViewModel model)
        {
            Cpu cooler = new Cpu()
            {
                CacheSize = model.CacheSize,
                GraphicsCore = model.GraphicsCore,
                Model = model.Modell,
                NumberOfCores = model.NumberOfCores,
                ProcessorLine = model.ProcessorLine,
                Core = model.Core,
                Socket = model.Socket,
                Speed = model.Speed,
                Price = model.Price,
                Image = model.Image != null ? $"images/Cpu/{model.Image.FileName}" : $"images/Cpu/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Cpu").Id
            };
            _cpuRepository.AddEntity(cooler);
            if (model.Image != null) _fileUploadService.Upload($"images\\Cpu", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _cpuRepository.FirstOrDefault(x => x.Id == id);
            _cpuRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _cpuRepository.FirstOrDefault(x => x.Id == id);
            CpuViewModel model = new CpuViewModel()
            {
                Id = entity.Id,
                CacheSize = entity.CacheSize,
                GraphicsCore = entity.GraphicsCore,
                Modell = entity.Model,
                Core = entity.Core,
                NumberOfCores = entity.NumberOfCores,
                ProcessorLine = entity.ProcessorLine,
                Socket = entity.Socket,
                Speed = entity.Speed,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(CpuViewModel model)
        {
            var entity = _cpuRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.CacheSize = model.CacheSize;
            entity.GraphicsCore = model.GraphicsCore;
            entity.Model = model.Modell;
            entity.NumberOfCores = model.NumberOfCores;
            entity.ProcessorLine = model.ProcessorLine;
            entity.Socket = model.Socket;
            entity.Core = model.Core;
            entity.Speed = model.Speed;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Cpu/{model.Image.FileName}" : entity.Image;
            _cpuRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Cpu", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_cpuRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_cpuRepository.GetAllEntities().ToList());
        }
    }
}