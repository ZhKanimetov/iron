﻿using Iron.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Repositories
{
    public interface ICoolerRepository : IItemRepository<Cooler>
    {
    }

    public class CoolerRepository : ItemRepository<Cooler>, ICoolerRepository
    {
        public CoolerRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Cooler;
        }
    }
}
