﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Hdd : IItem
    {
        public int Id { get; set; }
        public int Speed { get; set; }
        public string Ifs { get; set; }
        public string FormFactor { get; set; }
        public int Volume { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Volume} Гб {Speed} rpm";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Скорость вращения жесткого диска", Speed.ToString());
            dic.Add("Интерфейс", Ifs);
            dic.Add("Форм-фактор HDD", FormFactor);
            dic.Add("Объем жесткого диска", Volume.ToString());
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
