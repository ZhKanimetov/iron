﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class Cooler : IItem
    {
        public int Id { get; set; }
        public string Size { get; set; }
        public int Amount { get; set; }
        public int NoiseLevel { get; set; }
        public int Speed { get; set; }
        public int Heat { get; set; }
        public string Socket { get; set; }
        public string Manufacturer { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"{Manufacturer} {Speed} об/мин";
        }

        public Dictionary<string,string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Размеры вентилятора", Size);
            dic.Add("Количество вентиляторов", Amount.ToString());
            dic.Add("Уровень шума", NoiseLevel.ToString());
            dic.Add("Скорость вращения", Speed.ToString());
            dic.Add("Макс. тепловыделение процессора", Heat.ToString());
            dic.Add("Socket", Socket);
            dic.Add("Производитель", Manufacturer);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
