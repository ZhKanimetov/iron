﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Models;

namespace Iron.Repositories
{
    public interface IChooseRepository : IItemRepository<Choose>
    {
    }

    public class ChooseRepository : ItemRepository<Choose>, IChooseRepository
    { 
        public ChooseRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Choose;
        }
    }

}
