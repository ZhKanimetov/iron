﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Models
{
    [Serializable]
    public class WiFi : IItem
    {
        public int Id { get; set; }
        public string Standard { get; set; }
        public int Speed { get; set; }
        public string Antennas { get; set; }
        public string TypeAntennas { get; set; }
        public string Ifs { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public override string ToString()
        {
            return $"Wi-Fi адаптер {Speed} Мбит/с";
        }

        public Dictionary<string, string> Description()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Стандарт беспроводной связи", Standard);
            dic.Add("Макс. скорость беспроводного соединения", Speed.ToString());
            dic.Add("Внешняя антенна", Antennas);
            dic.Add("Тип внешней антенны", TypeAntennas);
            dic.Add("Интерфейс подключения", Ifs);
            dic.Add("Цена", Price.ToString());
            return dic;
        }
    }
}
