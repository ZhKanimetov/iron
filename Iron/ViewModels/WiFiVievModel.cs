﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Iron.ViewModels
{
    public class WiFiVievModel
    {
        public int Id { get; set; }
        [Display(Name = "Стандарт беспроводной связи")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Standard { get; set; }
        [Display(Name = "Макс. скорость беспроводного соединения")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Speed { get; set; }
        [Display(Name = "Внешняя антенна")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Antennas { get; set; }
        [Display(Name = "Тип внешней антенны")]
        [Required(ErrorMessage = "Заполните поле")]
        public string TypeAntennas { get; set; }
        [Display(Name = "Интерфейс подключения")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Ifs { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
    }
}
