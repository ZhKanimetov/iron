﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Iron.Controllers
{
    public class MbController : Controller, IEntityController
    {
        private readonly IMbRepository _mbRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public MbController(IMbRepository mbRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _mbRepository = mbRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(MbViewModel model)
        {
            var entity = new Mb()
            {
                Chipset = model.Chipset,
                ExpansionSlots = model.ExpansionSlots,
                FormFactor = model.FormFactor,
                MaxMemory = model.MaxMemory,
                MemoryType = model.MemoryType,
                Network = model.Network,
                RearConnectors = model.RearConnectors,
                Slots = model.Slots,
                Socket = model.Socket,
                Sound = model.Sound,
                Price = model.Price,
                Image = model.Image != null ? $"images/Mb/{model.Image.FileName}" : $"images/Mb/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Mb").Id
            };
            _mbRepository.AddEntity(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Mb", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _mbRepository.FirstOrDefault(x => x.Id == id);
            _mbRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var mb = _mbRepository.FirstOrDefault(x => x.Id == id);
            MbViewModel model = new MbViewModel()
            {
                Id = mb.Id,
                Chipset = mb.Chipset,
                ExpansionSlots = mb.ExpansionSlots,
                FormFactor = mb.FormFactor,
                MaxMemory = mb.MaxMemory,
                MemoryType = mb.MemoryType,
                Network = mb.Network,
                RearConnectors = mb.RearConnectors,
                Slots = mb.Slots,
                Socket = mb.Socket,
                Sound = mb.Sound,
                Price = mb.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(MbViewModel model)
        {
            var entity = _mbRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.Chipset = model.Chipset;
            entity.ExpansionSlots = model.ExpansionSlots;
            entity.FormFactor = model.FormFactor;
            entity.MaxMemory = model.MaxMemory;
            entity.MemoryType = model.MemoryType;
            entity.Network = model.Network;
            entity.RearConnectors = model.RearConnectors;
            entity.Slots = model.Slots;
            entity.Socket = model.Socket;
            entity.Sound = model.Sound;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Mb/{model.Image.FileName}" : entity.Image;
            _mbRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Mb", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_mbRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_mbRepository.GetAllEntities().ToList());
        }
    }
}