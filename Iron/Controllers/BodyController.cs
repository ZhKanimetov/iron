﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iron.Interfaces;
using Iron.Models;
using Iron.Repositories;
using Iron.Services;
using Iron.ViewModels;
using Microsoft.AspNetCore.Mvc;
namespace Iron.Controllers
{
    public class BodyController : Controller, IEntityController
    {
        private readonly IBodyRepository _bodyRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly FileUploadService _fileUploadService;

        public BodyController(IBodyRepository bodyRepository, FileUploadService fileUploadService, ICategoryRepository categoryRepository)
        {
            _bodyRepository = bodyRepository;
            _fileUploadService = fileUploadService;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(BodyViewModel model)
        {
            Body body = new Body()
            {
                FormFactor = model.FormFactor,
                Color = model.FormFactor,
                Dimensions = model.Dimensions,
                Ifs = model.Ifs,
                Manufacturer = model.Manufacturer,
                Material = model.Material,
                Type = model.Type,
                Price = model.Price,
                Image = model.Image != null ? $"images/Body/{model.Image.FileName}" : $"images/Body/noImage.jpg",
                CategoryId = _categoryRepository.FirstOrDefault(x => x.ControllerName == "Body").Id
            };
            _bodyRepository.AddEntity(body);
            if (model.Image != null) _fileUploadService.Upload($"images\\Body", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var entity = _bodyRepository.FirstOrDefault(x => x.Id == id);
            _bodyRepository.RemoveEntity(entity);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var entity = _bodyRepository.FirstOrDefault(x => x.Id == id);
            BodyViewModel model = new BodyViewModel()
            {
                Id = entity.Id,
                FormFactor = entity.FormFactor,
                Color = entity.FormFactor,
                Dimensions = entity.Dimensions,
                Ifs = entity.Ifs,
                Manufacturer = entity.Manufacturer,
                Material = entity.Material,
                Type = entity.Type,
                Price = entity.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BodyViewModel model)
        {
            var entity = _bodyRepository.FirstOrDefault(x => x.Id == model.Id);
            entity.FormFactor = model.FormFactor;
            entity.Color = model.FormFactor;
            entity.Dimensions = model.Dimensions;
            entity.Ifs = model.Ifs;
            entity.Manufacturer = model.Manufacturer;
            entity.Material = model.Material;
            entity.Type = model.Type;
            entity.Price = model.Price;
            entity.Image = model.Image != null ? $"images/Body/{model.Image.FileName}" : entity.Image;
            _bodyRepository.Update(entity);
            if (model.Image != null) _fileUploadService.Upload($"images\\Body", model.Image.FileName, model.Image);
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View(_bodyRepository.GetAllEntities());
        }

        public IActionResult ToSelect()
        {
            return PartialView(_bodyRepository.GetAllEntities().ToList());
        }
    }
}