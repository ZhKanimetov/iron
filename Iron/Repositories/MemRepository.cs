﻿using Iron.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.Repositories
{
    public interface IMemRepository : IItemRepository<Mem>
    {
    }

    public class MemRepository : ItemRepository<Mem>, IMemRepository
    {
        public MemRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Mem;
        }
    }
}
