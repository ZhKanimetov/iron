﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.ViewModels
{
    public class MemViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Тип памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public string MemoryType { get; set; }
        [Display(Name = "Тактовая частота памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Speed { get; set; }
        [Display(Name = "Объем памяти")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Memory { get; set; }
        [Display(Name = "Socket")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Socket { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }

        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
    }
}
