﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Iron.ViewModels
{
    public class CpuViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Линейка процессора")]
        [Required(ErrorMessage = "Заполните поле")]
        public string ProcessorLine { get; set; }
        [Display(Name = "Модель")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Modell { get; set; }
        [Display(Name = "Socket")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Socket { get; set; }
        [Display(Name = "Ядро")]
        [Required(ErrorMessage = "Заполните поле")]
        public string Core { get; set; }
        [Display(Name = "Количество ядер")]
        [Required(ErrorMessage = "Заполните поле")]
        public int NumberOfCores { get; set; }
        [Display(Name = "Тактовая частота процессора")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Speed { get; set; }
        [Display(Name = "Объем кэша")]
        [Required(ErrorMessage = "Заполните поле")]
        public int CacheSize { get; set; }
        [Display(Name = "Графическое ядро")]
        [Required(ErrorMessage = "Заполните поле")]
        public string GraphicsCore { get; set; }
        [Display(Name = "Фото")]
        public IFormFile Image { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Заполните поле")]
        public int Price { get; set; }
    }
}
